# Bento Responses #

The bunch of response formatters which transforms your WebApi output according to Microsoft REST API Guidelines.

### Usage ###

See how to [get started](https://bitbucket.org/floxdc/bento-responses/wiki/Getting%20Started) on docs.

### Documentation ###

All the documentation is located on the our [wiki](https://bitbucket.org/floxdc/bento-responses/wiki/Home) page.

### Community & Support ###

Need help with something that the docs aren't providing an answer to? Visit our [Issues page](https://bitbucket.org/floxdc/bento-responses/issues) and join in the conversation.

### Contributing ###

We'd love for you to contribute to the Bento Responses project!

### License & Credits###

This project is licensed under MIT [license](https://bitbucket.org/floxdc/bento-responses/src/master/LICENSE.MD).