﻿using System;
using System.Collections;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Extensions.Primitives;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json;

namespace FloxDc.Bento.Responses
{
    [DataContract(Name = "collectionResponse")]
    public class CollectionResponse<T> : IActionResult where T : ICollection
    {
        /// <summary>
        /// Represents a collection according to Microsoft REST API Guidelines.
        /// </summary>
        /// <param name="value">An underlying collection. <see cref="Value"/></param>
        /// <param name="nextLink">A paging link. <see cref="NextLink"/></param>
        public CollectionResponse(T value, string nextLink = null)
        {
            if (value.Equals(default(T)))
                throw new ArgumentNullException(nameof(value));

            NextLink = nextLink;
            Value = value;
        }

        
        public async Task ExecuteResultAsync(ActionContext context)
        {
            var segment = new StringSegment("application/json");
            var contentType = new MediaTypeCollection {new MediaTypeHeaderValue(segment) };

            var result = new ObjectResult(context)
            {
                ContentTypes = contentType,
                DeclaredType = typeof(CollectionResponse<>),
                StatusCode = StatusCodes.Status200OK,
                Value = this
            };

            await result.ExecuteResultAsync(context);
        }


        /// <summary>
        /// A paging link for big collections.
        /// </summary>
        [DataMember(Name = "@nextLink")]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate, NullValueHandling = NullValueHandling.Ignore)]
        public string NextLink { get; }

        /// <summary>
        /// An underlying collection. Required.
        /// </summary>
        [DataMember(Name = "value")]
        public T Value { get; }
    }
}
