﻿using System;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace FloxDc.Bento.Responses
{
    [DataContract(Name = "errorDetails")]
    public class ErrorDetails
    {
        /// <summary>
        /// Details about specific error that led to this reported error.
        /// </summary>
        /// <param name="code"></param>
        /// <param name="message"></param>
        /// <param name="target"></param>
        public ErrorDetails(string code, string message, string target = null)
        {
            Code = code;
            Message = message;
            Target = target;
        }

        /// <inheritdoc cref="ErrorDetails"/>
        public ErrorDetails(string code, Exception exception) : this(code, exception.Message, exception.Source) { }

        /// <inheritdoc cref="ErrorDetails"/>
        public ErrorDetails(Exception exception) : this(exception.ToString(), exception.Message, exception.Source) { }


        /// <summary>
        /// One of a server-defined set of error codes.
        /// </summary>
        [DataMember(Name = "code")]
        public string Code { get; }

        /// <summary>
        /// A human-readable representation of the error.
        /// </summary>
        [DataMember(Name = "message")]
        public string Message { get; }

        /// <summary>
        /// The target of the error.
        /// </summary>
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate, NullValueHandling = NullValueHandling.Ignore)]
        [DataMember(Name = "target")]
        public string Target { get; }
    }
}