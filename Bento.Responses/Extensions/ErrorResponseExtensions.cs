﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace FloxDc.Bento.Responses.Extensions
{
    public static class ErrorResponseExtensions
    {
        /// <summary>
        /// Creates a response from provided parameters.
        /// </summary>
        /// <typeparam name="TEnum"></typeparam>
        /// <param name="code">One of a server-defined set of error codes.</param>
        /// <param name="target">An array of details about specific errors that led to this reported error. <see cref="ErrorResponse.Target" /></param>
        /// <param name="details">The target of the error. <see cref="ErrorResponse.Details" /></param>
        /// <returns></returns>
        public static ErrorResponse Create<TEnum>(TEnum code, string target, ICollection<ErrorDetails> details) where TEnum : Enum
            => CreateFromEnum(code, target, details);


        /// <summary>
        /// Creates a response from provided parameters.
        /// </summary>
        /// <typeparam name="TEnum"></typeparam>
        /// <param name="code">One of a server-defined set of error codes.</param>
        /// <param name="details">The target of the error. <see cref="ErrorResponse.Details" /></param>
        /// <returns></returns>
        public static ErrorResponse Create<TEnum>(TEnum code, ICollection<ErrorDetails> details = null) where TEnum : Enum
            => CreateFromEnum(code, null, details);


        /// <summary>
        /// Creates a response from provided parameters.
        /// </summary>
        /// <typeparam name="TEnum"></typeparam>
        /// <param name="code">One of a server-defined set of error codes.</param>
        /// <param name="target">An array of details about specific errors that led to this reported error. <see cref="ErrorResponse.Target" /></param>
        /// <returns></returns>
        public static ErrorResponse Create<TEnum>(TEnum code, string target = null) where TEnum : Enum
            => CreateFromEnum(code, target, null);


        private static ErrorResponse CreateFromEnum<TEnum>(TEnum errorCode, string target, ICollection<ErrorDetails> details) where TEnum : Enum
        {
            var member = errorCode.ToString();
            var code = member;
            var message = string.Empty;

            var memberInfo = typeof(TEnum).GetMember(member);
            foreach (var attribute in memberInfo[0].GetCustomAttributes(false))
            {
                switch (attribute)
                {
                    case DescriptionAttribute descriptionAttribute:
                        message = descriptionAttribute.Description;
                        break;
                    case DisplayNameAttribute displayNameAttribute:
                        code = displayNameAttribute.DisplayName;
                        break;
                }
            }

            return new ErrorResponse(code, message, target, details);
        }
    }
}
