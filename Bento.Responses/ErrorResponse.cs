﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace FloxDc.Bento.Responses
{
    [DataContract(Name = "errorResponse") ]
    public partial class ErrorResponse : IActionResult
    {
        #region ctor

        /// <summary>
        /// Represents an error according to Microsoft REST API Guidelines.
        /// </summary>
        /// <param name="code">One of a server-defined set of error codes. Required.</param>
        /// <param name="message">A human-readable representation of the error. Required.</param>
        /// <param name="target">The target of the error.</param>
        /// <param name="details">An array of details about specific errors that led to this reported error.</param>
        /// <param name="exception">An error.</param>
        /// <exception cref="ArgumentNullException"></exception>
        public ErrorResponse(string code, string message, string target = null, ICollection<ErrorDetails> details = null, Exception exception = null)
        {
            if (string.IsNullOrWhiteSpace(code))
                throw new ArgumentNullException(nameof(code));

            if (string.IsNullOrWhiteSpace(message))
                throw new ArgumentNullException(nameof(message));

            Code = code;
            Message = message;
            Target = target;
            Details = details;
            InternalError = exception != null
                ? new InnerError(exception)
                : null;
        }


        /// <inheritdoc cref="ErrorResponse"/>
        public ErrorResponse(Exception exception) : this(exception.GetType().Name, exception.Message, exception.Source, null, exception) { }


        ///<inheritdoc cref="ErrorResponse"/>
        public ErrorResponse(string code, Exception exception) : this(code, exception.Message, exception.Source, null, exception) { }


        ///<inheritdoc cref="ErrorResponse"/>
        public ErrorResponse(string code, string message, Exception exception = null, ICollection<ErrorDetails> details = null) : this(code, message, exception?.Source, details, exception) { }


        internal ErrorResponse(bool isProduction, Exception exception)
        {
            Code = exception.GetType().Name;
            Details = null;
            Message = exception.Message;
            Target = exception.Source;
            InternalError = !isProduction
                ? new InnerError(exception)
                : null;
        }

        #endregion


        public async Task ExecuteResultAsync(ActionContext context)
        {
            var result = new ObjectResult(InternalError ?? (object) Code)
            {
                StatusCode = InternalError != null 
                    ? StatusCodes.Status500InternalServerError 
                    : StatusCodes.Status400BadRequest,
                Value = this
            };

            await result.ExecuteResultAsync(context);
        }


        public override string ToString()
            => JsonConvert.SerializeObject(this);


        /// <summary>
        /// One of a server-defined set of error codes. Required.
        /// </summary>
        [DataMember(Name = "code")]
        public string Code { get; }

        /// <summary>
        /// An array of details about specific errors that led to this reported error.
        /// </summary>
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate, NullValueHandling = NullValueHandling.Ignore)]
        [DataMember(Name = "details")]
        public ICollection<ErrorDetails> Details { get; }

        /// <summary>
        /// An object containing more specific information than the current object about the error.
        /// </summary>
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate, NullValueHandling = NullValueHandling.Ignore)]
        [DataMember(Name = "innererror")]
        public InnerError InternalError { get; }

        /// <summary>
        /// A human-readable representation of the error. Required.
        /// </summary>
        [DataMember(Name = "message")]
        public string Message { get; }

        /// <summary>
        /// The target of the error.
        /// </summary>
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate, NullValueHandling = NullValueHandling.Ignore)]
        [DataMember(Name = "target")]
        public string Target { get; }
    }
}