﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;

namespace FloxDc.Bento.Responses.Middleware
{
    public static class ExceptionHandlerMiddlewareExtensions
    {
        /// <summary>
        /// Adds a middleware to the pipeline that will handle exceptions and format them as <see cref="ErrorResponse"/>.
        /// Based on top of a standard UseExceptionHandler extension.
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="env"></param>
        /// <returns></returns>
        public static IApplicationBuilder UseBentoExceptionHandler(this IApplicationBuilder builder, IHostingEnvironment env)
            => builder.UseExceptionHandler(new ExceptionHandlerOptions
            {
                ExceptionHandler = async context =>
                {
                    if (context.Response.HasStarted)
                        return;

                    context.Response.ContentType = "application/json";
                    context.Response.StatusCode = StatusCodes.Status500InternalServerError;
                    var feature = context.Features.Get<IExceptionHandlerFeature>();
                    var exception = feature.Error;
                    if (exception is null)
                        return;

                    await context.Response.WriteAsync(new ErrorResponse(env.IsProduction(), exception).ToString());
                }
            });
    }
}
