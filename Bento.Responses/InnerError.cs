﻿using System;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace FloxDc.Bento.Responses
{
    public partial class ErrorResponse
    {
        [DataContract(Name = "innerError")]
        public class InnerError
        {
            internal InnerError(Exception exception)
            {
                Code = exception.GetType().Name;
                StackTrace = exception.StackTrace;
                InternalError = exception.InnerException != null
                    ? new InnerError(exception.InnerException)
                    : null;
            }


            /// <summary>
            /// A more specific error code than was provided by the containing error.
            /// </summary>
            [DataMember(Name = "code")]
            public string Code { get; }

            /// <summary>
            /// An object containing more specific information than the current object about the error.
            /// </summary>
            [DataMember(Name = "innererror")]
            public InnerError InternalError { get; }

            [JsonProperty(DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate, NullValueHandling = NullValueHandling.Ignore)]
            [DataMember(Name = "stacktrace")]
            public string StackTrace { get; }
        }
    }
}